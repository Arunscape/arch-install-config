# This is a script to backup my configs

# i3 config
#cp ~/.config/i3/config ~/Downloads/arch-install-config/configs/home/.config/i3/config

# sway config
cp ~/.config/sway/config ~/Downloads/arch-install-config/configs/home/.config/sway/config

# xinitrc
cp ~/.xinitrc ~/Downloads/arch-install-config/configs/home/.xinitrc

# xresources
cp ~/.Xresources ~/Downloads/arch-install-config/configs/home/.Xresources

# vim
cp ~/.vimrc ~/Downloads/arch-install-config/configs/home/.vimrc

#libinput gestures
cp ~/.config/libinput-gestures.conf ~/Downloads/arch-install-config/configs/home/.config/libinput-gestures.conf 

# kitty
cp ~/.config/kitty/kitty.conf ~/Downloads/arch-install-config/configs/home/.config/kitty/kitty.conf
