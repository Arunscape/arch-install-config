## Reload changes after changing Xresources (for urxvt)
```bash
xrdb -load ~/.Xresources
```
